﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

type Natural = Zero | Succ of Natural 

let Pred = function
    | Zero -> Zero
    | Succ n -> n

let rec applyn n f v = 
    match n with
    | Zero -> v
    | Succ n -> applyn n f (f v)

let rec hop o a b =
    match o with 
    | Zero -> a
    | Succ Zero -> applyn b Succ a
    | Succ o -> applyn (Pred b) (hop o a) a
    
let One = Succ Zero
let Two = Succ One
let Three = Succ Two

let (+) = hop One
let (*) = hop Two
let (^) = hop Three

[<EntryPoint>]
let main argv = 
    printfn "%A" (Three + Two)
    0 // return an integer exit code
